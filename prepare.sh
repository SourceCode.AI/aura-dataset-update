which aws
aws_present=$?

if [ $aws_present -ne 0 ]; then
  pip install awscli
fi

aws --version
