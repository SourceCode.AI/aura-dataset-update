SELECT
  COUNT(*) AS downloads,
  file.project AS package_name
FROM
  `bigquery-public-data.pypi.file_downloads`
WHERE
  -- Only query the last 30 days of history
  DATE(timestamp)
    BETWEEN DATE_SUB(CURRENT_DATE(), INTERVAL 30 DAY)
    AND CURRENT_DATE()
GROUP BY
  file.project
ORDER BY
  downloads DESC
