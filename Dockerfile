FROM ubuntu:latest

ENV DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Prague

RUN apt-get update && \
    apt-get install -y curl python3 awscli jq &&\
    rm -rf /var/lib/apt/lists/*

RUN mkdir /app
WORKDIR /app

RUN curl https://sdk.cloud.google.com > install_google_sdk.sh &&\
    chmod +x install_google_sdk.sh &&\
    ./install_google_sdk.sh --disable-prompts && \
    /root/google-cloud-sdk/bin/bq |head &&\
    aws --version
