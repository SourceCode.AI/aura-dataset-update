#!/bin/bash
set -euo pipefail

/root/google-cloud-sdk/bin/gcloud auth activate-service-account --key-file=${GCLOUD_IDENTITY_FILE}
/root/google-cloud-sdk/bin/bq ls
/root/google-cloud-sdk/bin/bq query --format=json --nouse_legacy_sql -n 9999999 "$(cat pypi_dataset_query.sql)">raw_pypi_stats.json
cat raw_pypi_stats.json|jq .[] -c>pypi_stats.json
