def norm_pkg: .|sub("[-_.]+"; "-")|ascii_downcase;
def parse_pkg: (capture("(?<d>[-a-zA-Z0-9_.]+)*") |.d |norm_pkg);

[.[]
| select(.dependencies != null)
| (.dependencies[] |= parse_pkg)
| .name as $name
| .dependencies[] as $d
| {n: $name, d: $d}
]
| [group_by(.d)[]
| {(.[0].d): ([(.[]| .n)]|unique)}]
| reduce .[] as $in ({}; . + $in)
