#!/bin/bash
set -Eeuo pipefail

PREV=$(pwd)
WORKDIR=$(mktemp -d -t aura_dataset_generation_XXXXXXXXXXXXXXXXX)

# check if tmp dir was created
if [[ ! "$WORKDIR" || ! -d "$WORKDIR" ]]; then
  echo "Could not create temp dir"
  exit 1
fi


cleanup() {
  if [[ -d "$WORKDIR" ]]; then
    rm -rf $WORKDIR || true
    echo "Deleted temporary directory ${WORKDIR}"
  fi
}

trap cleanup EXIT

cp pypi_stats.json reverse_dependencies.json dependency_list.json pypi_package_list.txt ${WORKDIR}/
cd $WORKDIR

tar -cvzf aura_dataset.tgz pypi_stats.json reverse_dependencies.json

gzip --keep --best --stdout reverse_dependencies.json>reverse_dependencies.gz
gzip --keep --best --stdout pypi_stats.json>pypi_download_stats.gz
gzip --keep --best --stdout dependency_list.json>dependency_list.gz
gzip --keep --best --stdout pypi_package_list.txt>pypi_package_list.gz

md5sum *.gz *.json pypi_package_list.txt>md5_checksums.txt

echo "Generated content:"
ls -lah
cat md5_checksums.txt

aws s3 sync ./ s3://cdn.sourcecode.ai/aura/ --acl public-read --exclude "*" --include "*gz" --include "md5_checksums.txt"

cd $PREV && cleanup
