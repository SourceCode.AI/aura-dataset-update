#!/bin/bash
set -euo pipefail

export SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
: "${DATAROOT:=/volume1/bandersnatch}"  # Set the default value
: "${PYPI_STATS_OUTFILE:=pypi_stats.json}"
: "${GCLOUD_IDENTITY_FILE:=/etc/gitlab-runner/aura-dataset-google-auth.json}"
: "${DATASET_DOCKER_IMAGE:=sourcecodeai/aura-dataset-updater:latest}"
export WEBDIR=${DATAROOT}/mirror/web
cd $DATAROOT


sync_mirror(){
  /usr/bin/flock -w 0 sync_lock docker run --rm  -v ${DATAROOT}:/data pypa/bandersnatch bandersnatch -c /data/bandersnatch.conf mirror | tee sync.log.stdout
}


generate_mirror_data(){
  cd $WEBDIR
  echo "Generating a TXT file with a list of all packages"
  ls ${WEBDIR}/json/>${DATAROOT}/pypi_package_list.txt
  echo "Generating dependency file"
  cd ${WEBDIR}/json
  cat ${DATAROOT}/pypi_package_list.txt|parallel -j5 -I% --xargs 'cat %|jq "{name: .info.name, dependencies: .info.requires_dist}" -c'>${DATAROOT}/dependency_list.json
  echo "Generating reverse dependencies"
  cd $DATAROOT
  cat ${DATAROOT}/dependency_list.json|jq -R "fromjson?" -c|jq --slurp -f "${SCRIPTDIR}/gen_dependencies.jq">reverse_dependencies.json
}


generate_pypi_dataset() {
  if [ ! -f "${GCLOUD_IDENTITY_FILE}" ]; then
    echo "Google identity file ${GCLOUD_IDENTITY_FILE} does not exists!">>/dev/stderr;
    exit 1;
  fi;

  docker pull ${DATASET_DOCKER_IMAGE}

  if [ ! -f "${PYPI_STATS_OUTFILE}" ] || test "`find "${PYPI_STATS_OUTFILE}" -mtime +3`"; then
    echo "Generating pypi download stats dataset"

    docker run --rm \
           -v ${GCLOUD_IDENTITY_FILE}:/gcloud_identity.json:ro \
           -v "$(pwd)":/data \
           -v "${CI_PROJECT_DIR}":/repo \
           -e GCLOUD_IDENTITY_FILE=/gcloud_identity.json\
           -w /data \
           $DATASET_DOCKER_IMAGE bash /repo/gen_pypi_stats.sh
  else
    echo "PyPI download stats is recent, skipping dataset generation"
  fi;
}


upload_dataset() {
  docker pull ${DATASET_DOCKER_IMAGE}

  docker run --rm \
         -v "$(pwd)":/data \
         -v "${CI_PROJECT_DIR}":/repo \
         -e AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID} \
         -e AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY} \
         -w /data \
         $DATASET_DOCKER_IMAGE bash /repo/upload_files.sh
}



sync_mirror
generate_pypi_dataset
upload_dataset


#if [ $? -ne 0 ]; then
#        echo "Already running, Skipping"
#else
#        cd $WEBDIR
#        echo "Generating a TXT file with a list of all packages"
#        ls ${WEBDIR}/json/>${DATAROOT}/pypi_package_list.txt
#        echo "Generating dependency file"
#        cd ${WEBDIR}/json
#        cat ${DATAROOT}/pypi_package_list.txt|parallel -j5 -I% --xargs 'cat %|jq "{name: .info.name, dependencies: .info.requires_dist}" -c'>${DATAROOT}/dependency_list.json
#        echo "Generating reverse dependencies"
#        cd $DATAROOT
#        cat ${DATAROOT}/dependency_list.json|jq -R "fromjson?" -c|jq --slurp -f ${SCRIPTDIR}/gen_dependencies.jq>reverse_dependencies.json
#        echo "Mirror sync finished"
#        cd $DATAROOT
#        bash ${SCRIPTDIR}/gen_pypi_stats.sh
#        bash ${SCRIPTDIR}/upload_files.sh
#fi
